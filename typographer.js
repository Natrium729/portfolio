/* The following function replaces straight quotation marks and apostrophes with curvy ones. Luckily, I don't have any opening single quotation marks, so the implementation is kept simple:
- Replace all straight apostrophes by curvy ones.
- Replace quotation marks preceded by a space with an opening quotation mark.
- Replace quotation marks followed by a space with an closing quotation mark. */

/**
 * Enhance the typography of the given string. Only replaces straight quotation marks
 * and apostrophes by curvy ones at the moment.
 * @param {string} text - The string whose typography will be enhanced.
 */
function enhanceTypography(text) {
	text = text
		.replace(/'|&#39;|&apos;/g, "’")
		.replace(/(\s)("|&quot;)/g, "$1“")
		.replace(/("|&quot;)(\s)/g, "”$2")

	// Log eventual unprocessed characters.
	if (text.match(/"|&quot;/)?.length) {
		console.warn("NON-PROCESSED QUOTE: ", text)
	}
	return text
}


/**
 * A PostHTML plug-in to replace straight quotation marks and apostrophes with curvy ones.
 */
module.exports.posthtmlTypographer = function (tree) {
	tree.walk(node => {
		if (node.content === undefined || node.tag === "code") {
			return node
		}

		if (typeof node.content === "string") {
			node.content = enhanceTypography(value)
		} else {
			node.content = node.content.map(value => {
				if (typeof value !== "string") {
					return value
				}
				return enhanceTypography(value)
			})
		}

		return node
	})

	return tree
}
