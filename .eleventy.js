const posthtml = require("posthtml")

const { posthtmlTypographer } = require("./typographer")


/**
 * Sort a collection of projects to get the wanted order.
 * @param {Array} arr - The array to sort.
 */
function sortProjectsByOrderProperty(arr) {
	// The project's ids in the wanted order.
	const ids = [
		"projects/coryphelle",
		"projects/vscode-inform6",
		"projects/inform-7-translation",
		"projects/portfolio",
		"projects/fiction-interactive-fr",
		"projects/la-princesse-speculaire",
		"projects/xyzzanie",
		"projects/dont-wake-up",
		"projects/le-temple-naga",
		"projects/running-out-of-sugar",
	]
	// Put the orders in a map id -> order.
	let orders = new Map();
	for (let i = 0; i < ids.length; i++) {
		const id = ids[i]
		orders.set(id, i)
	}
	return arr.sort((a, b) => {
		const idA = a.data?.id
		const idB = b.data?.id
		if (!orders.has(idA)) {
			console.error("ERROR: Missing order for ", idA)
		}
		if (!orders.has(idB)) {
			console.error("ERROR: Missing order for ", idB)
		}
		return (orders.get(idA)) - (orders.get(idB))
	})
}


module.exports = function(eleventyConfig) {
	// Passthrough
	eleventyConfig.addPassthroughCopy("src/images")

	// Custom filters
	eleventyConfig.addFilter("protectEmail", function(val) {
		return val.replace("@", "🐌")
	})
	eleventyConfig.addFilter("imageUrl", function(url) {
		return eleventyConfig.getFilter("url")("/images/" + url)
	})

	// Collections
	eleventyConfig.addCollection("all_en", function(collectionApi) {
		return collectionApi.getAll().filter(item => {
			return item.data?.lang === "en"
		})
	})
	eleventyConfig.addCollection("all_fr", function(collectionApi) {
		return collectionApi.getAll().filter(item => {
			return item.data?.lang === "fr"
		})
	})
	eleventyConfig.addCollection("projects_en", function(collectionApi) {
		return sortProjectsByOrderProperty(collectionApi.getFilteredByGlob("./src/en/projects/*.md"))
	})
	eleventyConfig.addCollection("projects_fr", function(collectionApi) {
		return sortProjectsByOrderProperty(collectionApi.getFilteredByGlob("./src/fr/projects/*.md"))
	})

	// Transforms
	eleventyConfig.addTransform("typographer", async (content, outputPath) => {
		if (!outputPath.endsWith(".html")) {
			return content
		}
		return (await posthtml([posthtmlTypographer]).process(content)).html
	})

	return {
		dir: {
			input: "src",
			output: "public"
		},
		markdownTemplateEngine: "njk",
		pathPrefix: "/portfolio/"
	}
}
