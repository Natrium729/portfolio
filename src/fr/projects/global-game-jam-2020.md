---
tags: projects
eleventyExcludeFromCollections: true
id: projects/global-game-jam-2020
name: Participation à la Global Game Jam 2020
categories: [Unity, C#, Game Design]
thumbnail: global-game-jam-2020/thumbnail.png
headerImage: global-game-jam-2020/screenshot.jpg
homepage: https://globalgamejam.org/2020/games/untilted-spaceship-game-3
---

J'ai participé à la Global Game Jam 2020 à Montréal.

5 autres personnes et moi avons créé un jeu pendant un week-end. Le but de ce jeu est de réarranger des cassettes afin de recréer une mélodie donnée.

Comme le jeu est avant tout musical et que toutes les instructions sont données par des pictogrammes et autres illustrations, nous avons utilisé le *diversifier* « language-independence ». (La Global Game Jam propose chaque année une liste de *diversifiers*, contraintes que l'on peut appliquer à son projet.)

J'ai été programmeur sur ce projet (contrôles, système de niveau), utilisant **Unity** (scripting en **C#**).

## Capture d'écran

[![Capture d'écran de ma participation à la Global Game Jam 2020.]({{ headerImage | imageUrl }})]({{ headerImage | imageUrl }})

## Liens

- [Page du jeu sur le site de la Global Game Jam]({{ homepage }})
