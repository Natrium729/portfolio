---
tags: projects
id: projects/inform-7-translation
name: Traduction d'Inform 7 en français
categories: [Inform 7]
thumbnail: inform-7-translation/thumbnail.png
headerImage: inform-7-translation/screenshot.png
repo: https://bitbucket.org/informfr/i7-french-language
---

Je suis le mainteneur principal de la traduction française d'**Inform 7**.

Ce projet open-source a deux objectifs.

Le premier est de permettre aux auteurs d'écrire des fictions interactives à analyseur syntaxique (« parser ») en français plutôt qu'en anglais. Pour cela, l'extension traduit toutes les réponses et les commandes par défaut des jeux produits par Inform 7 afin de permettre une expérience en français pour le joueur.

Inform 7 étant un langage de programmation naturel, le second but est de permettre aux auteurs d'écrire *leur code source* dans un langage le plus proche possible du français.

## Liens

- [Code source]({{ repo }})
