---
tags: projects
id: projects/dont-wake-up
name: Don't wake up!
categories: [Unity, C#, Game design]
thumbnail: dont-wake-up/thumbnail.png
headerImage: dont-wake-up/screenshot.jpg
---

Dans ce shoot 'em up, arpentez la planète afin de détruire les nuages orageux qui envahissent vos rêves.

À cause du monde sphérique, les projectiles ennemis s'accumulent au fur et à mesure. Et si vous ratez un tir, votre propre missile devient un nouveau projectile à éviter !

*Don't wake up* a été créé avec **Unity** (scripting en **C#**) en 4 jours, par une équipe de 4 personnes. J'ai principalement été programmeur sur ce projet (3C, comportement des ennemis, progression), ainsi que game designer (comportement des ennemis, progression).

## Vidéo

<p><video controls poster="{{ headerImage | imageUrl }}">
	<source src="http://ulukos.com/wp-content/uploads/2020/09/dont-wake-up-video.mp4" type="video/mp4">
	Votre navigateur ne supporte pas les vidéos.
</video></p>
