---
tags: projects
id: projects/vscode-inform6
name: Extension Inform 6 pour VS Code
categories: [TypeScript]
thumbnail: vscode-inform6/thumbnail.png
headerImage: vscode-inform6/screenshot.png
repo: https://gitlab.com/Natrium729/vscode-inform6
---

Cette extension de VS Code y ajoute la coloration syntaxique pour le langage de programmation Inform 6, ainsi que diverses commandes facilitant le développement de jeux dans ce langage (compilation de la source et lancement automatique de l'exécutable venant d'être créé, affichage des erreurs dans le code…).

Ce projet est écrit en **TypeScript**.

## Capture d'écran

[![Capture d'écran d'un fichier Inform 6 ouvert avec VS Code.]({{ headerImage | imageUrl }})]({{ headerImage | imageUrl }})

## Liens

- [Code source]({{ repo }})
