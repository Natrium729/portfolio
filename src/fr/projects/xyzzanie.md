---
tags: projects
id: projects/xyzzanie
name: Xyzzanie
categories: [Python]
thumbnail: xyzzanie/thumbnail.png
headerImage: xyzzanie/screenshot.png
repo: https://gitlab.com/Natrium729/xyzzanie
---

Xyzzanie est un bot Discord permettant de jouer à des fictions interactives à analyseur syntaxique (« parser ») de manière collaborative.

Il récupère les commandes que les joueurs tapent dans Discord, les soumet au jeu en cours et poste la réponse du jeu dans Discord.

Ce bot est écrit en **Python**.

## Liens

- [Code source]({{ repo }})
