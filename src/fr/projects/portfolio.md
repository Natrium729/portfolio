---
tags: projects
id: projects/portfolio
name: Ce portfolio
categories: [HTML, CSS]
thumbnail: portfolio/thumbnail.png
headerImage: portfolio/screenshot.jpg
repo: https://gitlab.com/Natrium729/portfolio
---

Le portfolio que vous lisez en ce moment même est une création personnelle. Il est écrit en **HTML**, **CSS** et utilise le générateur de site statique **Eleventy** avec **Nunjucks**.

## Liens

- [Code source]({{ repo }})
