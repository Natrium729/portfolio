---
tags: projects
eleventyExcludeFromCollections: true
id: projects/le-temple-naga
name: Le Temple nâga
categories: [Fiction interactive, Inform 7]
thumbnail: le-temple-naga/thumbnail.png
headerImage: le-temple-naga/screenshot.png
itchio: le-temple-naga
repo: https://gitlab.com/Natrium729/le-temple-naga
---

Trouvez, parmi les ruines d'une île déserte, un moyen de vaincre un ancien démon qui s'est réveillé.

*Le Temple nâga* est une courte fiction interactive où le joueur contrôle le protagoniste en tapant des commandes, comme dans les jeux d'aventure textuels « à l'ancienne ».


J'ai écrit *le Temple nâga* avec **Inform 7**.

## Capture d'écran

[![Capture d'écran du Temple nâga.]({{ headerImage | imageUrl }})]({{ headerImage | imageUrl }})

## Liens

- [Jouer en ligne sur itch.io]({{ myself.itchio }}{{ itchio }})
- [Code source]({{ repo }})
