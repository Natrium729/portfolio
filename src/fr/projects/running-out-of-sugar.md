---
tags: projects
id: projects/running-out-of-sugar
name: Running out of sugar
categories: [Defold, Lua, Game design]
thumbnail: running-out-of-sugar/thumbnail.png
headerImage: running-out-of-sugar/header.png
itchio: running-out-of-sugar
repo: https://gitlab.com/Natrium729/running-out-of-sugar
---

Dans *Running out of sugar*, mangez les sucreries qui tombent du ciel afin de faire remonter votre énergie.

Ce jeu est ma participation pour la Ludum Dare 39 (en 2017). Il a été fait en 72 heures.

*Running out of sugar* a été créé avec le moteur de jeu **Defold** (scripting en **Lua**) par deux personnes. J'ai fait l'entièreté de la programmation et du game design sur ce projet.

## Capture d'écran

{% set screenshot = "running-out-of-sugar/screenshot.png" | imageUrl %}

[![Capture d'écran de Running out of sugar.]({{ screenshot }})]({{ screenshot }})

## Liens

- [Jouer en ligne sur itch.io]({{ myself.itchio }}{{ itchio }})
- [Code source]({{ repo }})
