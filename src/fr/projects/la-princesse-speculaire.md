---
tags: projects
id: projects/la-princesse-speculaire
name: La Princesse spéculaire
categories: [Fiction interactive, Dialog]
thumbnail: la-princesse-speculaire/thumbnail.png
headerImage: la-princesse-speculaire/screenshot.png
itchio: la-princesse-speculaire
repo: https://gitlab.com/Natrium729/la-princesse-speculaire
---

*La Princesse spéculaire* est une fiction interactive où le joueur contrôle la protagoniste en tapant des commandes, comme dans les jeux d'aventure textuels « à l'ancienne ».

Ce projet a été ma participation au concours de fiction interactive francophone 2022 organisé par *Fiction-interactive.fr*. J'ai terminé 4<sup>e</sup> au classement général, et 3<sup>e</sup> dans la catégorie « écriture » et dans la catégorie « technique ».

J'ai écrit *la Princesse spéculaire* avec **Dialog**, un langage de création de fictions interactives inspiré d'Inform 7 et de Prolog.

## Capture d'écran

[![Capture d'écran de la Princesse spéculaire.]({{ headerImage | imageUrl }})]({{ headerImage | imageUrl }})

## Liens

- [Jouer en ligne sur itch.io]({{ myself.itchio }}{{ itchio }})
- [Code source]({{ repo }})
