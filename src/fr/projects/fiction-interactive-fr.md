---
tags: projects
id: projects/fiction-interactive-fr
categories: [Rédaction d'articles, WordPress]
name: Fiction-interactive.fr
thumbnail: fiction-interactive-fr/thumbnail.png
headerImage: fiction-interactive-fr/screenshot.jpg
homepage: http://www.fiction-interactive.fr/
---

*Fiction-interactive.fr* est le plus important site francophone sur les fictions interactives, les jeux vidéo textuels et la narration interactive en général.

J'ai initié la création de ce site pour remplacer le vieillissant IFiction-FR, son prédécesseur. J'ai coordonné le projet et configuré **WordPress**.

En plus de l'administration du site, j'y écris des articles — notamment des tutoriels expliquant comment créer des fictions interactives avec divers logiciels et langages de programmation —, et je relis et corrige les articles des autres contributeurs avant leur parution.

## Liens

- [Voir le site]({{ homepage }})
