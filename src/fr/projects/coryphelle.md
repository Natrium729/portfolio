---
tags: projects
id: projects/coryphelle
name: Coryphelle
categories: [Rust]
# thumbnail: coryphelle/thumbnail.png
# headerImage: coryphelle/screenshot.png
repo: https://gitlab.com/Natrium729/coryphelle
---

Coryphelle est un interpréteur Glulx (une machine virtuelle dédiée aux fictions interactives) écrit en **Rust**.

Ce projet est toujours en cours de réalisation. L'un des buts est de compiler l'interpréteur en WebAssembly afin qu'il soit utilisable en ligne.

## Liens

- [Code source]({{ repo }})
