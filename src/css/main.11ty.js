/**
 * This JavaScript template file reads the css from _includes/css/index.css, processes
 * it with PostCSS, and returns the processed css.
 */

const fs = require("fs/promises")
const path = require("path")

const autoprefixer = require("autoprefixer")
const { browsersWithSupportForFeatures } = require("browserslist-generator")
const cssnano = require("cssnano")
const postcss = require("postcss")
const postcssImport = require("postcss-import")


const fromPath = path.join(__dirname, "../_includes/css/index.css")


module.exports = class {
	data() {
		return {
			permalink: "css/main.css"
		}
	}

	async render() {
		let css = ""
		try {
			css = await fs.readFile(fromPath, "utf8")
		} catch (err) {
			throw err
		}

		const browserslist = browsersWithSupportForFeatures("css-variables")

		const postcssPlugins = [
			postcssImport,
			autoprefixer({ overrideBrowserslist: browserslist }),
			cssnano({
				preset: [
					"default",
					{
						// A bug causes the `all` property to be moved, which results in problems.
						// See https://github.com/cssnano/cssnano/issues/1469
						// So we disable the CSS declaration sorter for the time being.
						// (In any case, I already sort the properties by hand :))
						cssDeclarationSorter: false,
					},
				]
			}),
		]
		const postcssResult = await postcss(postcssPlugins).process(css, { from: fromPath })
		for (const warning of postcssResult.warnings()) {
			console.warn(warning)
		}
		return postcssResult.css
	}
}
