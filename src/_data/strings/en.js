module.exports = {
	description: "Mathematics-physics graduate, interested in computer science, interactive storytelling and video games. Below, some of my past or current projects!",
	home: "Home",
	learnMore: "Learn more",
	contact: "Contact",
	resume: "Résumé",
	link: "Link",
}
