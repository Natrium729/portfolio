module.exports = {
	description: "Diplômé en mathématiques-physique, passionné par l'informatique, la narration interactive et les jeux vidéo. Ci-dessous, quelques-uns de mes projets passés ou en cours !",
	home: "Accueil",
	learnMore: "En savoir plus",
	contact: "Contact",
	resume: "CV",
	link: "Lien",
}
