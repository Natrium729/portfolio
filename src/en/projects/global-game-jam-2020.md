---
tags: projects
eleventyExcludeFromCollections: true
id: projects/global-game-jam-2020
name: Global Game Jam 2020 participation
categories: [Unity, C#, Game Design]
thumbnail: global-game-jam-2020/thumbnail.png
headerImage: global-game-jam-2020/screenshot.jpg
homepage: https://globalgamejam.org/2020/games/untilted-spaceship-game-3
---

I've taken part to the Global Game Jam 2020 in Montréal.

5 other people and me created a game during a weekend. The objective of this game is to rearrange cassettes to recreate a given melody.

Since the game is mainly musical, and all the instructions are conveyed with pictograms and illustrations, we used the "language-independence" diversifier. (The Global Game Jam offers each year a set of diversifiers, constraints that you can add to your project.)

I was programmer on this project (controls, level system), using **Unity** (scripting in **C#**).

## Screenshot

[![Screenshot of my participation to the Global Game Jam 2020.]({{ headerImage | imageUrl }})]({{ headerImage | imageUrl }})

## Links

- [The game's page on the Global Game Jam's site]({{ homepage }})
