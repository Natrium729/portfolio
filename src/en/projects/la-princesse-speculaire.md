---
tags: projects
id: projects/la-princesse-speculaire
name: La Princesse spéculaire
categories: [Interactive fiction, Dialog]
thumbnail: la-princesse-speculaire/thumbnail.png
headerImage: la-princesse-speculaire/screenshot.png
itchio: la-princesse-speculaire
repo: https://gitlab.com/Natrium729/la-princesse-speculaire
---

*La Princesse spéculaire* is an interactive fiction in French, where the player controls the protagonist by typing commands, like in "old-school" text adventures.

This project was my entry for the 2022 French interactive fiction competition organised by *Fiction-interactive.fr*. I placed 4<sup>th</sup> in the general category, and 3<sup>rd</sup> in the "writing" category and the "technical" category.

I wrote *la Princesse spéculaire* with **Dialog**, a programming language inspired by Inform 7 and Prolog to create interactive fictions.

## Screenshot

[![Capture d'écran de la Princesse spéculaire.]({{ headerImage | imageUrl }})]({{ headerImage | imageUrl }})

## Links

- [Play online on itch.io]({{ myself.itchio }}{{ itchio }})
- [Source code]({{ repo }})
