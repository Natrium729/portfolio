---
tags: projects
id: projects/running-out-of-sugar
name: Running out of sugar
categories: [Defold, Lua, Game design]
thumbnail: running-out-of-sugar/thumbnail.png
headerImage: running-out-of-sugar/header.png
itchio: running-out-of-sugar
repo: https://gitlab.com/Natrium729/running-out-of-sugar
---

In *Running out of sugar*, eat the candies falling from the sky to regain your energy.

This game is my entry for Ludum Dare 39 (in 2017). It was made in 72 hours.

*Running out of sugar* was created with the **Defold** game engine (scripting in **Lua**) by two people. I did the entirety of the programming and the game design on this project.

## Screenshot

{% set screenshot = "running-out-of-sugar/screenshot.png" | imageUrl %}

[![Screenshot of Running out of sugar.]({{ screenshot }})]({{ screenshot }})

## Links

- [Play online on itch.io]({{ myself.itchio }}{{ itchio }})
- [Source code]({{ repo }})
