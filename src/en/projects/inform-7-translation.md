---
tags: projects
id: projects/inform-7-translation
name: Inform 7 French translation
categories: [Inform 7]
thumbnail: inform-7-translation/thumbnail.png
headerImage: inform-7-translation/screenshot.png
repo: https://bitbucket.org/informfr/i7-french-language
---

I'm the main maintainer of the French translation of **Inform 7**.

This open-source project has two objectives.

The first one is to make it possible for authors to write parser interactive fictions in French rather than in English. The extension translates every default response and command of games produced by Inform 7 so that players can fully experience interactive fictions in French.

Inform 7 being a natural programming language, the second objective is to make it possible for authors to write *their source code* in a language as close as possible to French.

## Links

- [Source code]({{ repo }})
