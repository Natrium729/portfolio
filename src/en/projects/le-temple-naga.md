---
tags: projects
eleventyExcludeFromCollections: true
id: projects/le-temple-naga
name: Le Temple nâga
categories: [Interactive fiction, Inform 7]
thumbnail: le-temple-naga/thumbnail.png
headerImage: le-temple-naga/screenshot.png
itchio: le-temple-naga
repo: https://gitlab.com/Natrium729/le-temple-naga
---

Find, in the ruins of a desert island, a way to defeat an ancient demon that just awakened.

*Le Temple nâga* is a short interactive fiction in French, where the player controls the protagonist by typing commands, like in "old-school" text adventures.

I've written *le Temple nâga* with **Inform 7**.

## Screenshot

[![Screenshot of Le Temple nâga.]({{ headerImage | imageUrl }})]({{ headerImage | imageUrl }})

## Links

- [Play online on itch.io]({{ myself.itchio }}{{ itchio }})
- [Source code]({{ repo }})
