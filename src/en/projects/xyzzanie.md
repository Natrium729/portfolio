---
tags: projects
id: projects/xyzzanie
name: Xyzzanie
categories: [Python]
thumbnail: xyzzanie/thumbnail.png
headerImage: xyzzanie/screenshot.png
repo: https://gitlab.com/Natrium729/xyzzanie
---

Xyzzanie is a Discord bot for playing parser interactive fictions collaboratively.

It retrieves the commands players type in Discord, submits them to the game currently running and posts the game's response to Discord.

This bot is written in **Python**.

## Links

- [Source code]({{ repo }})
