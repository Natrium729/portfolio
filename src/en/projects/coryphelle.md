---
tags: projects
id: projects/coryphelle
name: Coryphelle
categories: [Rust]
# thumbnail: coryphelle/thumbnail.png
# headerImage: coryphelle/screenshot.png
repo: https://gitlab.com/Natrium729/coryphelle
---

Coryphelle is a Glulx interpreter (Glulx being a virtual machine dedicated to interactive fictions) written in **Rust**.

This project is still in progress. One of the aims is to compile the interpreter to WebAssembly so that it can be used online.

## Links

- [Source code]({{ repo }})
