---
tags: projects
id: projects/portfolio
name: This portfolio
categories: [HTML, CSS]
thumbnail: portfolio/thumbnail.png
headerImage: portfolio/screenshot.jpg
repo: https://gitlab.com/Natrium729/portfolio
---

The portfolio you're reading right now is a personal creation. It is written in **HTML**, **CSS** and uses the static site generator **Eleventy** with **Nunjucks**.

## Links

- [Source code]({{ repo }})
