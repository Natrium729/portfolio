---
tags: projects
id: projects/fiction-interactive-fr
categories: [Writing of articles, WordPress]
name: Fiction-interactive.fr
thumbnail: fiction-interactive-fr/thumbnail.png
headerImage: fiction-interactive-fr/screenshot.jpg
homepage: http://www.fiction-interactive.fr/
---

*Fiction-interactive.fr* is the biggest website in French about interactive fiction, textual video games and interactive storytelling in general.

I initiated the creation of the site to replace the ageing IFiction-FR, its predecessor. I coordinated the project and configured **WordPress**.

In addition to the site's administration, I write articles – notably tutorials explaining how to create interactive fictions with several systems and programming languages –, and I edit the articles of the other contributors.

## Links

- [See the site]({{ homepage }})
