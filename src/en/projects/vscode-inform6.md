---
tags: projects
id: projects/vscode-inform6
name: Inform 6 extension for VS Code
categories: [TypeScript]
thumbnail: vscode-inform6/thumbnail.png
headerImage: vscode-inform6/screenshot.png
repo: https://gitlab.com/Natrium729/vscode-inform6
---

This VS Code extension adds syntax highlighting for Inform 6 as well as multiple commands helping development of games in this language (compiling the source and opening automatically the resulting file, displaying the errors in the code…).

This project is written in **TypeScript**.

## Screenshot

[![Screenshot of an Inform 6 file opened with VS Code.]({{ headerImage | imageUrl }})]({{ headerImage | imageUrl }})

## Links

- [Source code]({{ repo }})
