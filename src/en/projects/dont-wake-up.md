---
tags: projects
id: projects/dont-wake-up
name: Don't wake up!
categories: [Unity, C#, Game design]
thumbnail: dont-wake-up/thumbnail.png
headerImage: dont-wake-up/screenshot.jpg
---

In this shoot 'em up, hover around the globe and destroy the stormy clouds overrunning your dreams.

Because the world is spherical, hostile projectiles accumulate gradually around the planet. And if you miss an enemy, your own missile becomes a new projectile to dodge!

*Don't wake up* was created with **Unity** (scripting in **C#**) in 4 days, by 4 people. I was mainly programmer on this project (3C, enemy behaviour, progress), as well as game designer (enemy behaviour, progress).

## Video

<p><video controls poster="{{ headerImage | imageUrl }}">
	<source src="http://ulukos.com/wp-content/uploads/2020/09/dont-wake-up-video.mp4" type="video/mp4">
	Your browser does not support videos.
</video><p>
